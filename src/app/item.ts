// quantities are normalized to 1 gram
export interface Item {
  name: string;
  type: string;          // fruit, vegetable, legume, fish
  pie: any[];
  kcal: number;          // kcal - calories
  gl: number;            // glycemic load unitless
  gi: number;            // glycemic index unitless
  fat: number;           // g
  sodium: number;        // mg
  potassium: number;     // mg
  carbohydrates: number; // g
  fiber: number;         // g
  sugar: number;         // g
  protein: number;       // g
  checked: boolean;
}
