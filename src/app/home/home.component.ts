import {AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Item} from '../item';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {ItemDataService} from '../item-data.service';
import {FilterItemService} from '../filter-item.service';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {shareReplay} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewChecked, AfterViewInit, OnInit {

  // isSmallScreen: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.XSmall])
  // .pipe(map(result => result.matches), shareReplay());
  items: Item[] = this.itemDataService.items;

  // displayedColumns: string[] = ['name', 'type', 'pie', 'calories', 'fat', 'carbohydrates', 'fiber', 'protein'];
  displayedColumns: string[] = ['name', 'kcal', 'gl', 'gi', 'pie', 'fat', 'carbohydrates', 'fiber', 'protein'];
  dataSource = new MatTableDataSource<Item>(this.items);
  hideColumn = false;
  selected = 'All';

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  @ViewChild(MatSort) sort: MatSort | undefined;

  constructor(private breakpointObserver: BreakpointObserver, private itemDataService: ItemDataService,
              private filterItemService: FilterItemService) {
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    this.dataSource.sort = this.sort;
    // @ts-ignore
    setTimeout(() => this.dataSource.paginator = this.paginator);
    shareReplay();
  }

  ngAfterViewChecked(): void {


    // this.itemDataService.$items.subscribe(res => {
    //   this.dataSource.data = res as Item[];
    // });
  }

  ngOnInit(): void {
    this.filterItemService.$criterion.subscribe(criterion => {
      this.dataSource.filter = criterion.trim().toLocaleLowerCase();
    });

    this.filterItemService.$clearToggle.subscribe((clear: boolean) => {
      this.selected = '';
    });
    // this.itemDataService.$items.subscribe(items => {
    //   this.itemDataService.items = items;
    // });
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      // Breakpoints.Medium,
      // Breakpoints.Large,
      // Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.breakpoints[Breakpoints.XSmall]) {
        this.displayedColumns = ['name', 'kcal', 'gl', 'gi', 'pie'];
        shareReplay();
      }
      if (result.breakpoints[Breakpoints.Small]) {
        this.displayedColumns = ['name', 'kcal', 'gl', 'gi', 'pie', 'fat', 'carbohydrates', 'fiber', 'protein'];
        shareReplay();
      }
    });
  }

  public filterItems = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public filterTypes = (value: string) => {
    this.dataSource.filter = '';
    this.filterItems(value);
    this.filterItemService.clearFilter();
  }

  public getAllItems = () => {
    // this.itemDataService.$items.subscribe(res => {
    //   this.dataSource.data = res as Item[];
    // });

    // this.itemDataService.getData('api/item').subscribe(res => {
    //     this.dataSource.data = res as Item[];
    //   })
  }

  public redirectToDetails = (id: string) => {

  }
}
