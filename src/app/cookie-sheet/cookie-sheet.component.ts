import {Component} from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-cookie-sheet',
  templateUrl: './cookie-sheet.component.html',
  styleUrls: ['./cookie-sheet.component.scss']
})
export class CookieSheetComponent {

  constructor(private bottomSheetRef: MatBottomSheetRef<CookieSheetComponent>, private cookieService: CookieService) {
  }

  agree(event: MouseEvent): void {
    this.cookieService.set('Domain', '');
    this.cookieService.set('agree', 'true');
    console.log('User agreed to terms: ' + this.cookieService.get('agree'));
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
