export interface Entry {
  name: string;
  description: string;
}
