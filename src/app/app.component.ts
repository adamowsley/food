import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
  ViewChild
} from '@angular/core';
import {CookieSheetComponent} from './cookie-sheet/cookie-sheet.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {Router, NavigationEnd} from '@angular/router';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {FormControl} from '@angular/forms';
import {MatSlideToggle} from '@angular/material/slide-toggle';
import {FilterItemService} from './filter-item.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentInit, OnInit {

  @HostBinding('class') className = '';
  @ViewChild('darkModeToggle', {static: false}) ref!: MatSlideToggle;

  agree = false;
  darkClassName = 'darkMode';
  filterValue = '';
  isSmallScreen: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.XSmall])
    .pipe(map(result => result.matches), shareReplay());
  showFilter = true;
  title = 'Working Title';
  toggleControl = new FormControl(false);

  constructor(private bottomSheet: MatBottomSheet, private breakpointObserver: BreakpointObserver,
              private changeDetectorRef: ChangeDetectorRef, private cookieService: CookieService,
              private filterItemService: FilterItemService, private router: Router) {
  }

  ngAfterContentInit(): void {
    this.changeDetectorRef.detectChanges();
    this.ref.checked = this.className === this.darkClassName;
  }

  ngOnInit(): void {
    if (this.cookieService.get('agree') === 'true') {
      this.agree = true;
    } else {
      // only display bottom banner if user did not previously agree
      this.bottomSheet.open(CookieSheetComponent);
    }
    // check for dark mode regardless of agreement; default empty string means light mode
    this.className = this.cookieService.get('darkMode');
    this.toggleControl.valueChanges.subscribe((darkMode) => {
      this.className = darkMode ? this.darkClassName : '';
      if (this.agree) {
        this.cookieService.set('darkMode', this.className);
      }
    });
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.showFilter = event.url === '/home';
      }
    });
    this.filterItemService.$clearFilterText.subscribe((clear: boolean) => {
      if (clear) {
        this.filterValue = '';
      }
    });
  }

  filterItems(value: string): void {
    this.filterItemService.addCriterion(value);
    this.filterItemService.clearToggle();
  }
}
