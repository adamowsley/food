import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterItemService {

  private filterDataSubject = new BehaviorSubject<string>('');
  $criterion = this.filterDataSubject.asObservable();

  private clearDataSubject = new BehaviorSubject<boolean>(false);
  $clearFilterText = this.clearDataSubject.asObservable();

  private toggleDataSubject = new BehaviorSubject<boolean>(false);
  $clearToggle = this.toggleDataSubject.asObservable();

  constructor() { }

  addCriterion(criterion: string): void {
    this.filterDataSubject.next(criterion);
  }

  clearFilter(): void {
    this.clearDataSubject.next(true);
  }

  clearToggle(): void {
    this.toggleDataSubject.next(true);
  }
}
