import { Injectable } from '@angular/core';
import {Entry} from "./entry";
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EntryDataService {

  entries: Entry[] = [
    {name: 'Amino Acids', description: '- are the building blocks of all proteins. Amino acids combine in various sequences to construct all of the proteins required for metabolism and grown. Our bodies are only capable of manufacturing 12 of the 20 amino acids; the remaining eight are derived from the foods the we consume. The body absorbs amino acids through the small intestine and distributes them via the blood to the rest of body.'},
    {name: 'Bioavailability', description: '- is the ease in which substances are absorbed from our digestive tracts into our bloodstream. The higher the bioavailability of a substance, the greater the absorption by our body.'},
    {name: 'Bran', description: '- is the outer layer of any particular grain. It contains fiber, vitamins, and minerals. Bran is destroyed during the refinement process used to manufacture products such as white bread, etc.'},
    {name: 'Calcium', description: '- is a mineral that our bodies need to maintain strong bones, muscle movement, and nerve functions.'},
    {name: 'Calorie', description: '- is a unit of energy contained in food. It is the energy needed to raise the temperature of 1 gram of water 1 °C which is about 4.19 joules.'},
    {name: 'Carbohydrate', description: '- is a macronutrient that is a readily converted energy source.'},
    {name: 'Cellulose', description: '- is an insoluble plant fiber.'},
    {name: 'Cholesterol', description: '- is a sterol that is produced by the body but is also found in animal products such as dairy, poultry, eggs, and other meats.'},
    {name: 'Cholesterol', description: '- is a sterol that is produced by the body but is also found in animal products such as dairy, poultry, eggs, and other meats.'},
    {name: 'Complex Carbohydrate', description: '- is a....'},
    {name: 'Complementary Proteins', description: '- are contain some essential amino acids but requires other amino acids from other foods in order for our body to process them properly.'},
    {name: 'Complete Proteins', description: '- contain all of the essential amino acids required by the body and do not require other nutrients for their use by the body.'},
    {name: 'Diabetes', description: '- is a disease caused by the inability of the body to control the amount of glucose in the blood. Type 2 diabetes occurs when our body cells develop resistance to insulin, and type 1 diabetes occurs from the body\'s inability to produce insulin in the pancreas.'},
    {name: 'Enzyme', description: '- is....'},
    {name: 'Fats', description: '- are lipids that are an essential source of energy in the body.'},
    {name: 'Fatty Acid', description: '- is....'},
    {name: 'Fiber', description: '- is a plant-based food that the body can not break down. It keeps the digestive system clean by passing through the body undigested; thereby, removing cholesterol and harmful substances out of the body.'},
    {name: 'Glucose', description: '- is a simple sugar derived from the breakdown of carbohydrates by the body; it is a major source of energy for the body and especially the brain.'},
    {name: 'Gluten', description: '- is....'},
    {name: 'Glycemic Index', description: '- is a measure of the rate at which ingested food causes the level of glucose in the blood to rise.'},
    {name: 'Glycemic Load', description: '- is....'},
    {name: 'HDL', description: '- is....'},
    {name: 'LDL', description: '- is....'},
    {name: 'Metabolism', description: '- is....'},
    {name: 'Nutrient', description: '- is....'},
    {name: 'Peptide Bond', description: '- is the chemical bond between carbon and nitrogen in a peptide linkage.'},
    {name: 'Pescatarian', description: '- is a person whose diet consists solely of fish, fruits, vegetables, and other plant-based foods. A pescatarian does not eat other meats.'},
    {name: 'Protein', description: '- is any of various naturally occurring complex substances consisting of amino acids joined by peptide bonds; they contain carbon, hydrogen, nitrogen, oxygen, and sometimes sulfur and other elements such as phosphorus or iron. They also contain essential enzymes, hormones, and antibodies.'},
    {name: 'Sodium', description: '- is....'},
    {name: 'Sterols', description: '- are any of various steroid alcohols that are widely abundant in animal and plant lipids. Cholesterol is a steroid alcohol.'},
    {name: 'Tofu', description: '- is a bean curd made from soybean milk mixed with coagulants that separate the protein from the water in soybean milk.'},
    {name: 'Vegetarian', description: '- is a person whose diet consists solely of vegetables, fruit, grains, nuts, and other plant-based foods. A vegetarian does not eat meat but sometimes may eat eggs or other dairy products.'},
  ];

  private dataSubject = new BehaviorSubject<Entry[]>(this.entries);

  $entries = this.dataSubject.asObservable();

  constructor(private http: HttpClient) {
  }



}
