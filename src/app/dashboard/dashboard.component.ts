import {Component} from '@angular/core';
import {Breakpoints, BreakpointObserver} from '@angular/cdk/layout';

//item quantities are normalized to 1 gram
export interface FoodItem {
  name: string;
  calories: number;     //kcal
  fat: number;          //g
  sodium: number;       //mg
  potassium: number;    //mg
  carbohydrate: number; //g
  fiber: number;        //g
  sugar: number;        //g
  protein: number;      //g
  flipped: boolean;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  // flipped: Array<boolean> = Array();

  cols: number = 4;

  cards: FoodItem[] = [];
  //   = [
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  //   {name: 'Asparagus', calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrate: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, flipped: false},
  // ];

  // dataSource = new MatTableDataSource<FoodItem>(this.cards);
  displayColumns: string[] = ['Calories', 'Fat', 'Sodium', 'Potassium', 'Carbohydrate', 'Fiber', 'Sugar', 'Protein'];
  // sortedData: FoodItem[] | undefined;

  // @ts-ignore
  // @ViewChild(MatSort) sort: MatSort | undefined;

  // @ts-ignore
  /** Based on the screen size, switch from standard to one column per row */
  // cards = this.breakpointObserver.observe(Breakpoints.Web).pipe(
  //   map(({matches}) => {
  //     // console.log(Breakpoints);
  //     // if (matches) {
  //       return [
  //         {title: 'Card 1', cols: 2, rows: 1, flipped: false},
  //         {title: 'Card 2', cols: 2, rows: 1, flipped: false},
  //         {title: 'Card 3', cols: 2, rows: 1, flipped: false},
  //         {title: 'Card 4', cols: 2, rows: 1, flipped: false}
        // ];
      // }
      // return [
      //   {title: 'Card 1', cols: 1, rows: 1, flipped: false},
      //   {title: 'Card 2', cols: 2, rows: 1, flipped: false},
      //   {title: 'Card 3', cols: 1, rows: 2, flipped: false},
      //   {title: 'Card 4', cols: 2, rows: 2, flipped: false}
      // ];
  //   })
  // );

  constructor(private breakpointObserver: BreakpointObserver) {

    // // @ts-ignore
    // this.dataSource.sort = this.sort;

    // this.sortedData = this.cards.slice();

    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.breakpoints[Breakpoints.XSmall]) {
        this.cols = 2;
      }
      if (result.breakpoints[Breakpoints.Small]) {
        this.cols = 3;
      }
      if (result.breakpoints[Breakpoints.Medium]) {
        this.cols = 4;
      }
      if (result.breakpoints[Breakpoints.Large]) {
        this.cols = 5;
      }
      if (result.breakpoints[Breakpoints.XLarge]) {
        this.cols = 6;
      }
    });

    // ngAfterContentChecked(): void {
    //   this.dataSource.sort = this.sort;
    // }

    // sortData(sort: Sort):void {
    //   const data = this.cards.slice();
    //   if (!sort.active || sort.direction === '') {
    //     this.sortedData = data;
    //     return;
    //   }
    //
    //   this.sortedData = data.sort((a, b) => {
    //     const isAsc = sort.direction === 'asc';
    //     switch (sort.active) {
    //       case 'title': return compare(a.title, b.title, isAsc);
    //       case 'calories': return compare(a.calories, b.calories, isAsc);
    //       case 'fat': return compare(a.fat, b.fat, isAsc);
    //       case 'carbs': return compare(a.carbohydrate, b.carbohydrate, isAsc);
    //       case 'protein': return compare(a.protein, b.protein, isAsc);
    //       default: return 0;
    //     }
    //   });
    // }

  }
}

// function compare(a: number | string, b: number | string, isAsc: boolean) {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }
