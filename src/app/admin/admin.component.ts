import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminComponent implements OnDestroy, OnInit {

  // editordoc = jsonDoc;

  jsonDoc = '';

  // editor: Editor;
  // form = new FormGroup({
  //   editorContent: new FormControl(this.jsonDoc, Validators.required())
  // });

  html: string;

  constructor() {
    // this.editor = new Editor();
    this.html = '';
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    // this.editor.destroy();
  }

  // get doc(): AbstractControl {
  //   return this.form.get('editorContent') as AbstractControl;
  // }

  save(): void {
    // console.log(this.doc.value);
  }

}
