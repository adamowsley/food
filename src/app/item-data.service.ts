import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Item} from './item';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItemDataService {

  items: Item[] = [
    {name: 'Apple', type: 'fruit', pie: [5, 50, 20, 25], kcal: 55, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Apricot', type: 'fruit', pie: [15, 22, 42, 18], kcal: 75, gl: 44, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Artichoke', type: 'fruit', pie: [12, 29, 26, 31], kcal: 43, gl: 23, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Arugula', type: 'vegetable', pie: [5, 30, 40, 25], kcal: 11, gl: 25, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Asparagus', type: 'vegetable', pie: [9, 28, 23, 15], kcal: 22, gl: 7, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Avocado', type: 'fruit', pie: [5, 50, 20, 25], kcal: 89, gl: 54, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Banana', type: 'fruit', pie: [30, 20, 40, 10], kcal: 78, gl: 65, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Bass', type: 'fish', pie: [10, 25, 25, 40], kcal: 100, gl: 12, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Blackberry', type: 'fruit', pie: [5, 50, 20, 25], kcal: 34, gl: 8, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Blueberry', type: 'fruit', pie: [5, 50, 20, 25], kcal: 34, gl: 4, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Carrot', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 26, gl: 18, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Celery', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 6, gl: 21, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Cherry', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 22, gl: 34, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Chickpea', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 15, gl: 31, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Date', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 42, gl: 16, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Eggplant', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 30, gl: 7, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Ginger', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 11, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Lentil', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 9, gl: 14, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Navy Bean', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 11, gl: 3, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Onion', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 14, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Peanut', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 21, gl: 13, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Potato', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 43, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Sweet Potato', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 49, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Tomato', type: 'vegetable', pie: [5, 50, 20, 25], kcal: 25, gl: 9, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Watermelon', type: 'fruit', pie: [5, 50, 20, 25], kcal: 33, gl: 10, gi: 10,  fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
  ];

  private dataSubject = new BehaviorSubject<Item[]>(this.items);

  $items = this.dataSubject.asObservable();

  constructor(private http: HttpClient) {
    console.log('Production: ' + environment.production);
    console.log('URL: ' + environment.urlAddress);
  }

  addItem(item: Item): void {
    this.items.push(item);
    this.dataSubject.next(this.items);
  }

  private createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }

  public getData = (route: string) => {
    return this.items;
   // return this.dataSubject.next(this.items);
   //  return this.$items;
   //  return new Observable((observer: Observer<any>) => {
   //    observer.next(this.items);
   //    observer.complete();
   //  });
    // return this.http.get(this.createCompleteRoute(route, environment.urlAddress));
  }
}
