import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {Entry} from "../entry";
import {EntryDataService} from "../entry-data.service";

@Component({
  selector: 'app-glossary',
  templateUrl: './glossary.component.html',
  styleUrls: ['./glossary.component.scss']
})
export class GlossaryComponent implements OnInit {

  // cardLayout: any = {
  //   card: {cols: 1, rows: 1},
  //   chart: {cols: 2, rows: 2},
  //   columns: 2,
  //   rowHeight: '150px',
  //   table: {cols: 4, rows: 4},
  // };

  entries: Entry[] = this.entryDataService.entries;

  panelOpen = false;

  constructor(private breakpointObserver: BreakpointObserver, private entryDataService: EntryDataService) {
  }

  ngOnInit(): void {
    // this.breakpointObserver.observe([
    //   Breakpoints.XSmall,
    //   Breakpoints.Small,
    //   Breakpoints.Medium,
    //   Breakpoints.Large,
    //   Breakpoints.XLarge
    // ]).subscribe(result => {
    //   if (result.breakpoints[Breakpoints.XSmall]) {
    //     this.cardLayout = {
    //       card: {cols: 1, rows: 1},
    //       chart: {cols: 1, rows: 2},
    //       columns: 1,
    //       rowHeight: '200px',
    //       table: {cols: 1, rows: 4},
    //     };
    //     shareReplay();
    //   }
    //   if (result.breakpoints[Breakpoints.Small]) {
    //     this.cardLayout = {
    //       card: {cols: 1, rows: 1},
    //       chart: {cols: 2, rows: 2},
    //       columns: 2,
    //       rowHeight: '200px',
    //       table: {cols: 4, rows: 4},
    //     };
    //     shareReplay();
    //   }
    //   if (result.breakpoints[Breakpoints.Medium]) {
    //     this.cardLayout = {
    //       card: {cols: 1, rows: 1},
    //       chart: {cols: 2, rows: 2},
    //       columns: 3,
    //       rowHeight: '200px',
    //       table: {cols: 4, rows: 4},
    //     };
    //     shareReplay();
    //   }
    //   if (result.breakpoints[Breakpoints.Large]) {
    //     this.cardLayout = {
    //       card: {cols: 1, rows: 1},
    //       chart: {cols: 2, rows: 2},
    //       columns: 4,
    //       rowHeight: '240px',
    //       table: {cols: 4, rows: 4},
    //     };
    //     shareReplay();
    //   }
    // });
  }

}
