import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ChartOptions, ChartType} from 'chart.js';
import {
  BaseChartDirective,
  Label,
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip,
  SingleDataSet
} from 'ng2-charts';
import * as pluginLabels from 'chartjs-plugin-datalabels';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {shareReplay} from 'rxjs/operators';

@Component({
  selector: 'app-food-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
  public chartColors: Array<any> = [{backgroundColor: ['#FF6448', '#EACE66', '#9ACD32', '#AEAAAA']}];
  public height = 100;
  private isSmallScreen = false;
  public width = 80;
  @Input() pieChartData: SingleDataSet = [25, 25, 25, 25];
  public pieChartLegend = false;
  public pieChartLabels: Label[] = ['Fats', 'Carbs', 'Fiber', 'Protein'];
  public pieChartOptions: ChartOptions = PieChartComponent.createOptions();
  public pieChartPlugins: Array<any> = [
    {
      afterLayout: (chart: any) => {
        chart.legend.options.align = 'left';
        chart.legend.legendItems.forEach(
          (label: any) => {
            const value = chart.data.datasets[0].data[label.index];
            label.text = value + ' ' + label.text;
            // label.texts = value;
            return label;
          }
        );
      }
    },
    {
      beforeInit: (chart: any, options: any) => {
        // tslint:disable-next-line:typedef
        chart.legend.afterFit = function() {
          // add this to prevent legend text clipping off right side of rect
          this.width += 10;
        };
      }
    }];
  public pieChartType: ChartType = 'pie';

  private static createOptions(): ChartOptions {
    return {
      responsive: true,
      maintainAspectRatio: false,
      legend: {position: 'right'},
      plugins: {
        labels: {
          render: 'percentage',
          fontColor: ['black', 'black', 'black', 'black'],
          precision: 2
        }
      },
    };
  }

  constructor(private breakpointObserver: BreakpointObserver) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    // this.breakpointObserver.observe([
    //   Breakpoints.XSmall,
    //   Breakpoints.Small,
    //   Breakpoints.Medium,
    //   Breakpoints.Large,
    //   Breakpoints.XLarge
    // ]).subscribe(result => {
    //   if (result.breakpoints[Breakpoints.Small]) {
    //     console.log('small');
    //     this.isSmallScreen = true;
    //     this.height = 80;
    //     this.width = 80;
    //     // this.pieChartLegend = false;
    //     // shareReplay();
    //     this.pieChartOptions = { ...this.pieChartOptions };
    //   }
    //   if (result.breakpoints[Breakpoints.Medium]) {
    //     console.log('medium');
    //     this.isSmallScreen = false;
    //     this.height = 100;
    //     this.width = 100;
    //     // this.pieChartLegend = true;
    //     // shareReplay();
    //     this.pieChartOptions = { ...this.pieChartOptions };
    //   }
    //   if (result.breakpoints[Breakpoints.Large]) {
    //     console.log('medium');
    //     this.isSmallScreen = false;
    //     this.height = 120;
    //     this.width = 120;
    //     // this.pieChartLegend = true;
    //     // shareReplay();
    //     this.pieChartOptions = { ...this.pieChartOptions };
    //   }
    // });
  }
}
